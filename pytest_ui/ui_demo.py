import random
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
import time
from pytest_ui.page.basepage import BasePage
from selenium.webdriver.support import expected_conditions as EC


def test_login():
    driver = BasePage().get_driver()
    user_name = driver.find_element(By.XPATH,'//*[@id="mat-expansion-panel-header-1"]/span/label').text
    print(user_name)
    assert user_name == '登录用户名', '登录失败'


def test_add_blogs():
    driver = BasePage().get_driver()
    #显式等待 ,满足条件返回该元素的页面元素对象
    element1 = WebDriverWait(driver,30,0.5).until(EC.visibility_of_element_located((By.XPATH, '/html/body/app-root/app-main-layout/app-navbar/mat-toolbar/mat-toolbar-row/div[1]/a[1]/img')))
    print(element1)
    element1.click()
    # driver.find_element(By.XPATH, '/html/body/app-root/app-main-layout/app-navbar/mat-toolbar/mat-toolbar-row/div[1]/a[1]/img').click()
    element2 = WebDriverWait(driver,30,0.5).until(EC.visibility_of_element_located((By.ID, 'new_post_icon')))
    element2.click()
    # driver.find_element(By.ID, 'new_post_icon').click()
    #隐式等待
    driver.implicitly_wait(1)
    text = '这是我的第{}篇自动生成随笔'.format(random.randint(1,100))
    driver.find_element(By.ID, 'post-title').send_keys(text)
    driver.implicitly_wait(1)
    # driver.find_element(By.ID, 'tinymce"] ').send_keys('这是写的随笔内容，哈哈哈！')
    #切换到iframe
    iframe = driver.find_element(By.ID,'Editor_Edit_EditorBody_ifr')
    driver.switch_to.frame(iframe)
    driver.find_element(By.XPATH, '//*[@id="tinymce"]').send_keys('这是写的随笔内容，哈哈哈！')

    #切换回主文档
    driver.switch_to.default_content()
    #滚动到页面底部
    driver.execute_script("var q=document.documentElement.scrollTop=10000")
    publish_button=driver.find_element(By.XPATH, "/html/body/cnb-root/cnb-app-layout/div[2]/as-split/as-split-area[2]/div/div/cnb-spinner/div/cnb-post-editing-v2/cnb-post-editor/div[3]/cnb-spinner/div/cnb-submit-buttons/button[1]")
    driver.execute_script("arguments[0].click();",publish_button)
    time.sleep(1)
    # 退出
    driver.quit()


if __name__ == '__main__':
    test_login()

