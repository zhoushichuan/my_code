from pytest_ui.page.login_page_object import LoginPageObject


def login_page(user_name='2462824640@qq.com', passwd='shichuaN920807'):
    '''登录page'''
    pg = LoginPageObject()
    pg.input_username(user_name)
    pg.input_passwd(passwd)
    pg.submit_element()
    return pg

if __name__=='__main__':
    login_page()