
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
import time

class BasePage(object):
    '''基类'''
    def __init__(self):
        self.url = 'https://account.cnblogs.com/'
        self.driver = webdriver.Chrome()
        self.driver.maximize_window()
        self.driver.implicitly_wait(5)
        self.driver.get(self.url)

    # 重写元素定位方法
    def find_element(self, *loc):
        try:
            WebDriverWait(self.driver, 10).until(lambda driver:driver.find_element(*loc).is_displayed())
            return self.driver.find_element(*loc)
        except:
            print(u"%s 页面中未能找到 %s 元素" % (self, loc))

# if __name__=='__main__':
#     BasePage().get_driver()
