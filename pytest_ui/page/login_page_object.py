from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
import time

from pytest_ui.page.basepage import BasePage

class LoginPageObject(BasePage):
    '''登录page类'''
    #元素定位
    # user_name = (By.ID,'mat-input-0')
    # passwd = (By.ID,'mat-input-1')
    # submit_button = (By.XPATH,'/html/body/app-root/app-sign-in-layout/div/div/app-sign-in/app-content-container/div/div/div/form/div/button')

    def __init__(self):
        # self.url = 'https://account.cnblogs.com/'
        # self.driver = webdriver.Chrome()
        # self.driver.maximize_window()
        # self.driver.implicitly_wait(5)
        # self.driver.get(self.url)
        super().__init__()

    @property
    def username_element(self):
        return self.find_element(By.ID,'mat-input-0')

    @property
    def password_element(self):
        return self.find_element(By.ID,'mat-input-1')

    @property
    def submit_el(self):
        return self.find_element(By.XPATH,'/html/body/app-root/app-sign-in-layout/div/div/app-sign-in/app-content-container/div/div/div/form/div/button')

    def input_username(self,username):
        return self.username_element.send_keys(username)
    def input_passwd(self,password):
        return self.password_element.send_keys(password)
    def submit_element(self):
        return self.submit_el.click()
    def login_button_exist(self):
        try:
            self.driver.find_element(By.XPATH,'/html/body/app-root/app-sign-in-layout/div/div/app-sign-in/app-content-container/div/div/div/form/div/button')
        except NoSuchElementException as e:
            print(e)
            return False
        else:
            return True
    def login_username_text(self):
        try:
            text = self.driver.find_element(By.XPATH, '//*[@id="mat-expansion-panel-header-1"]/span/label').text
        except NoSuchElementException as e:
            print(e)
            return False
        else:
            return text


    def login_page(self, user_name='2462824640@qq.com', passwd='shichuaN920807'):
        '''登录page'''
        self.input_username(user_name)
        self.input_passwd(passwd)
        self.submit_element()
        # return driver

if __name__=='__main__':
    # driver = LoginPageObject()
    # driver.login_page()
    LoginPageObject().login_page()