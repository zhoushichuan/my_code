import random
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
import time
from pytest_ui.page.basepage import BasePage
from selenium.webdriver.support import expected_conditions as EC

from pytest_ui.page.login_page_action import login_page
from pytest_ui.page.login_page_object import LoginPageObject
import pytest


@pytest.mark.parametrize('username,passwd',[('2462824640@qq.com','123456'),('2462824641@qq.com','shichuaN920807')])
def test_error_account_login(username,passwd):
    pg = login_page(username,passwd)
    flag = pg.login_button_exist()
    assert flag == True, '未登录失败'

def test_corret_account_login():
    pg = login_page()
    user_name = pg.login_username_text()
    print(user_name)
    assert user_name == '登录用户名', '登录失败'

if __name__ == '__main__':
    test_error_account_login()

