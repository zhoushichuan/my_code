import smtplib
from email.mime.text import MIMEText

mail_host = 'smtp.oa.com'
sender_email = '2462824640@qq.com'

def send_email(subject,content,receivers_list):
    '''发送html邮件'''
    msg = MIMEText(content,_subtype='html',_charset='utf-8')
    msg['From'] = sender_email
    msg['Subject'] = u'%s'%subject
    msg['To'] = ','.join(receivers_list)
    try:
        s = smtplib.SMTP(mail_host,25) #端口25不变
        s.sendmail(sender_email,receivers_list,msg.as_string())
        s.close()
    except Exception as e:
        print('Exception:',e)
